variable "proxmox_hostname" {
  type = string
  description = "Proxmox host name or IP address"
  default = "192.168.0.102"
}

variable "proxmox_user" {
  type        = string
  description = "Proxmox User Name"
  default     = "root@pam"
}

variable "proxmox_password" {
  type        = string
  description = "Proxmox Password"
  sensitive = true
}

# variable "proxmox_token" {
#   type        = string
#   description = "Proxmox API Token"
# }

variable "proxmox_node" {
  type = string
  description = "Proxmox node in which the VM will be created"
  default = "proxmox"
}

variable "vm_id" {
  type = number
  description = "VM ID"
  default = 9004
}

variable "ssh_password" {
  type        = string
  description = "SSH Password"
  sensitive = true
}

variable "vm_name" {
  type        = string
  description = "VM Name"
  default     = "ubuntu-server-2204"
}

variable "iso_urls" {
  type        = list(string)
  description = "ISO URLs"
  default     = [
    "local:iso/ubuntu-22.04.1-live-server-amd64.iso",
    "https://releases.ubuntu.com/22.04/ubuntu-22.04.1-live-server-amd64.iso"
  ]
}

variable "iso_checksum" {
  type        = string
  description = "ISO Checksum"
  default = "10f19c5b2b8d6db711582e0e27f5116296c34fe4b313ba45f9b201a5007056cb"
}

variable "template_name" {
  type        = string
  description = "Template Name"
  default     =  "ubuntu-server-2204"
}

variable "template_description" {
  type        = string
  description = "Template Description"
  default     =   "proxmox vm based on ubuntu server 2204 edition"
}

variable "git_name" {
  type        = string
  description = "Git Name"
  default = "Tabrez Iqbal"
}

variable "git_email" {
  type        = string
  description = "Git Email"
  default = "tabrez@mailbox.org"
}

variable "gitconfig_url" {
  type        = string
  description = "Gitconfig URL"
  default = "https://gist.githubusercontent.com/tabrez/73446db8b9d562d2bdcf9d47d2adedc9/raw"
}

variable "ssh_private_key_path" {
  type        = string
  description = "SSH Private Key Path"
}

