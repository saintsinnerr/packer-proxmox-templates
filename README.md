# Create Proxmox VM Image for Ubuntu server 20.04.6 using Packer

* Set proxmox hostname/ip address, port number & username in `vars.pkr.hcl`
* Set ubuntu ssh username in `vars.pkr.hcl` & `http/user-data` (Default: ubuntu)
* Set proxmox user password & ubuntu ssh user password in `secrets.auto.pkrvars.hcl.example` and rename it to `secrets.auto.pkrvars.hcl`
* Generate a password-less ssh keypair if you don't have one already to be used by packer:

```sh
export KEY_NAME=nik
cat /dev/zero | ssh-keygen -t ed25519 -f ~/.ssh/"${KEY_NAME}" -C "non-interactive key" -q -N ""
```
Set `private_key_file_path` in `secrets.auto.pkrvars` to the full path of private key generated above
Set the above generated public key contents in `http/user-data`

* Adjust the URL of ubuntu iso if needed, it gets changed when a new minor version is released e.g. 20.04.7
* Run `packer init .` & `packer build .`

